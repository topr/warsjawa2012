package warsjawa2012.spockogroovy

import spock.lang.Specification
import spock.lang.Ignore

class Ex04aCollectionsSpecification extends Specification {

    def list = [1, 2, 3]
    def longList = [1, 2, 3, 4, 5]

    def set = list as Set
    def bigSet = longList as Set

    def collection = list

    def 'convenient bulk/set operations'() {
        expect:
        collection.containsAll(list)

        and:
        collection.disjoint([42])
        !collection.disjoint([3, 42])
    }

    def "use 'in' instead of '.contains'"() {

        expect:
        3 in longList //e.g.: if (element in list) { //...

        /*
        Exercise:
         check, that 42 is not in longList
         */
        and:
        true //exercise

    }

    def 'collection operators'() {
        when:
        def anotherCollection = collection + [4, 5]

        then:
        anotherCollection == longList
        collection == [1, 2, 3]
        !anotherCollection.is(collection)

        when:
        collection << 13

        then:
        collection == [1, 2, 3, 13]

        when:
        collection << 42 << 100

        then:
        collection == [1, 2, 3] + [13, 42, 100]

        /*
        Exercise: try subtracting (using 'i') some objects from a list, set and a map
         */
        and:
        true //exercise

    }

    /*
    Exercise:
     check out the '<<' '+' and '-' operators for lists
    */
    @Ignore('exercise')
    def 'collection operators for sets'() {
        expect:
        set + bigSet == 'exercise :)'
        //...















    }


    def foodPreferences = [alice: 'pasta', bob: 'steak']
    def foodPreferencesUpdate = [alice: 'sushi', carol: 'plants']

    /*
    Exercise:
     check out the '<<' '+' and '-' operators for maps (which, BTW, are NOT collections :))
    */
    @Ignore('exercise')
    def 'operators for maps'() {
        given:
        def (pasta, steak, sushi, plants) = ['pasta', 'steak', 'sushi', 'plants']

        expect:
        foodPreferences == [alice: pasta, bob: steak]
        foodPreferences + foodPreferencesUpdate == 'exercise :)'
        //...
















    }

    def 'handling maps is easy as well'() {
        given:
        def (pasta, steak, sushi) = ['pasta', 'steak', 'sushi']

        when:
        foodPreferences << [dave: sushi]

        then:
        foodPreferences['dave'] == sushi

        when:
        foodPreferences['dave'] = steak

        then:
        foodPreferences.dave == steak

        when:
        foodPreferences['dave'] += " and $pasta"
        foodPreferences.dave += " and $sushi"

        then:
        foodPreferences.dave == "$steak and $pasta and $sushi"
    }

    //FIXME objects by string by default in maps!!!


    def 'sorting is in-place by default'() {
        given:
        def list = [3, 1, 2]
        def set = list as Set

        when:
        list.sort()
        def setElementsSorted = set.sort()

        then:
        list == [1, 2, 3]
        setElementsSorted instanceof List
        setElementsSorted == list
    }

    def 'mutating the sorted list can be avoided'() {
        given:
        def list = [3, 1, 2]

        when:
        def sortedList = list.sort(false)

        then:
        sortedList == [1, 2, 3]
        sortedList != list
    }

    def 'flattening nested collections'() {
        expect:
        [[1], [[2], [3, 4], [5]]].flatten() == longList

        /*
        Exercise:
         how will flatten behave for a set of collections? What do you predict?
        */
        and:
        def setOfLists = [list, [2, 3], [[1]]] as Set
        //setOfLists.flatten() == 'exercise :)'
    }

    def "grep - the lazy man's findAll"() {
        def number = 1
        def string = '234'
        def sensibleNumber = 42
        def items = [number, sensibleNumber, string, 'foobar', [], [1, 2, 3]]

        expect:
        items.grep(number) == [number]
        items.grep(string)
        items.grep([number, sensibleNumber, 1000]) == [number, sensibleNumber]

        and:
        items.grep(~/\d{3}/) == [string]
        items.grep(~/\d+/) == [number, sensibleNumber, string]
        (~/\d+/).isCase(number)

        and:
        items.grep { it == number || it == string } == [number, string]
    }

    def 'combinations (or rather: cartesian products) are for free in Groovy'() {
        def words = [['nice', 'neat'], ['red', 'sad'], ['llama', 'alpaca', 'narwahl']]

        expect:
        words.combinations() == [
                ['nice', 'red', 'llama'],
                ['neat', 'red', 'llama'],
                ['nice', 'sad', 'llama'],
                ['neat', 'sad', 'llama'],
                ['nice', 'red', 'alpaca'],
                ['neat', 'red', 'alpaca'],
                ['nice', 'sad', 'alpaca'],
                ['neat', 'sad', 'alpaca'],
                ['nice', 'red', 'narwahl'],
                ['neat', 'red', 'narwahl'],
                ['nice', 'sad', 'narwahl'],
                ['neat', 'sad', 'narwahl'],
        ]

        /*
        QUIZ
        - what if words is a set? how will iteration order influence the combinations?
        */
    }


    def 'permutations are for free too!'() {
        given:
        def permutationsInSystematicOrder = [
                [1, 2, 3], [1, 3, 2], [2, 1, 3], [2, 3, 1], [3, 1, 2], [3, 2, 1]
        ]

        when:
        def permutations = list.permutations()

        then:
        permutations == permutationsInSystematicOrder as Set
        permutations instanceof HashSet


        when:
        def orderedPermutations = []
        list.eachPermutation { orderedPermutations << it }

        then:
        orderedPermutations == permutationsInSystematicOrder


        when:
        orderedPermutations = []
        set.eachPermutation { orderedPermutations << it }

        then:
        orderedPermutations == permutationsInSystematicOrder
        set instanceof LinkedHashSet
    }

    def 'collections can be intersected'() {

        expect:
        collection.intersect([2, 3, 42]) == [2, 3]


        and: 'in case of lists, elements from both collections' +
             '(and their order) are taken from the smaller one (or right)'

        list.intersect([3, 2, 1, 1]) == list
        [3, 2, 1, 1].intersect(list) == list
        list.intersect([3, 2, 1]) == [3, 2, 1]
        [3, 2, 1].intersect(list) == list


        and: 'in case of different collection types, the result is of type of' +
             'the bigger (or left) collection'

        [2, 3, 42, 100].intersect(set) == [2, 3]
        set.intersect([2, 3, 42, 100]) == [2, 3]
        list.intersect([2, 3, 42] as Set) == [2, 3]
        set.intersect([2, 3, 42]) == [2, 3] as Set
    }
}
