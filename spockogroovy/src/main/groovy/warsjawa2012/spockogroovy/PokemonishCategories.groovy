package warsjawa2012.spockogroovy

class PokemonishCategories {

    @Category(String)
    static class Generated {
        String pokemonish(int n = 2) {
            PokemonishCategories.toPokemonish(this, n)
        }
    }

    static class ByHand {
        static pokemonish(String self, int n = 2) {
            PokemonishCategories.toPokemonish(self, n)
        }
    }

    static String toPokemonish(String string, Integer n) {
        def result = ''
        string.eachWithIndex { String letter, int i ->
            result += i % n ? letter.toLowerCase() : letter.toUpperCase()
        }
        result
    }
}
